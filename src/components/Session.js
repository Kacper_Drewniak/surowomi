import React from "react";
import {Row, Col} from "reactstrap";
import Button from "./button";

export const Session = ({sessionsText,onClick}) => {



  const SessionItem = ({text, headerText,onClick}) => <div className="session">
    <div className="session-left"/>
    <Row className="session-center">
      <Col sm={4}>

      </Col>
      <Col sm={8} className="session-center-content ">
        <h4 className="font-weight-bolder font-size-32">{headerText}</h4>
        <p>
          {text}
        </p>
        <Button buttonText="Umów sie" onClick={onClick}/>
      </Col>
    </Row>
    <div className="session-right"/>

  </div>

  return <>
    {sessionsText.map(({description,id, headerText}, index) => <SessionItem headerText={headerText} text={description} number={index + 1} onClick={()=>onClick(id)}/>)}
  </>
}