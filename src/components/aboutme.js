import React from 'react'
import {Row, Col} from 'reactstrap'
import {ContentWithImage} from "./contentWithImage";

export const AboutMe = () => {

  return <>


    <ContentWithImage
      imgSrc={'/images/about1.jpg'}
      headerText="Alicja Maziarz"
      description={<>Ciągle odkrywam <strong>nowe </strong>głębie, <strong>nowe </strong> jakości i <strong>nowe </strong> dary życia. I ciągle czuję, że dotykam jedynie
        niewielkiej części potencjału … potencjału, który w sobie mam.
        <br/>
        <br/>

        Jestem certyfikowaną nauczycielką <strong>jogi</strong> dla kobiet w ciąży i mama baby joga. <strong>Kocham</strong> pracę z kobietami i dziećmi,
        dzielenie się swoją energią, wiedzą i doświadczeniem.
        Kundalini jogę <strong>pokochałam</strong> od pierwszego… ćwiczenia. Doskonale pamiętam pierwsze zajęcia po których poczułam
        niesamowitą energię i <strong>radość życia</strong>. Od tej pory to mój codzienny rytuał. Pojawiła się ona w moim życiu gdy po 12
        latach prowadzenia własnej działalności gospodarczej zamknęłam tamten etap mojego życia i poszłam za głosem
        <strong> serca</strong>. Moje życie zaczęło się zmieniać, przyszło surowe odżywanie, posty sokowe i suche, pojawiali się nowi
        wspaniali ludzie, korzystne rozwiązania, wyjątkowe okazje.<br/> I pojawił się <strong>FLOW</strong>, ten nurt życia, który trwa,
        prowadzi mnie bo mam odwagę mu na to pozwolić. Moje życie stało się <strong>pasją</strong>.</>}
    />

    <ContentWithImage
      revert
      imgSrc={'/images/about2.png'}
      headerText="Troszkę o mnie"
      description={<>
        Jestem certyfikowaną terapeutką <strong>Theta Healing</strong> oraz certyfiowanym <strong>Irydologiem</strong>, prekursorską holistycznego
        podejścia do zdrowia.
        Mam swój kanał na instagramie pod nazwą <strong>surowo.mi</strong> gdzie dzielę się swoją drogą  <strong>surowego odżywiania</strong>, inspiruję
        przepisami oraz prowadzę wywiady z cudownymi osobami, które dzielą się swoją historią dotyczącą zmiany
        odżywiania na surowe. Jestem adwokatem transerfingu i prawa założenia.
        <br/>
        <br/>

        Jestem współzałożycielką procesu <strong>„Jestem Boską Doskonałością”</strong> który stworzyłyśmy wraz z Madzią Burczyk-Gwóźdź
        dzięki któremu wiele osób <strong>pokochało</strong> siebie i swoje życie  na nowo.
        <br/> <br/>


        Prywatnie jestem mamą dwóch czworonogów Loli i Teosia, <strong>kocham</strong> się śmiać, <strong>kocham</strong> zwierzęta, podróże, spędzanie
        czasu na łonie natury, chodzenie boso po lesie, taniec i  śpiew.
      </>}
    />
  </>
}
