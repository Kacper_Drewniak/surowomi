import React from "react";
import ChevronRightIcon from '@mui/icons-material/ChevronRight';

const Button = ({buttonText, onClick,classLabel=""}) => <button onClick={onClick}
                                                  className={`social-content-button font-size-16 font-color-white font-weight-bolder ${classLabel}`}>

  {buttonText} <ChevronRightIcon style={{fill: "white"}}/>
</button>

export default Button