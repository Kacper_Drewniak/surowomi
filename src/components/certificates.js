import React,{useState,useEffect} from 'react'
import {Col, Row} from "reactstrap";
import useWindowDimensions from './../services/useWindowDimensions'

export const Certificates = () => {



  const [width,setWidth] = useState()
  console.log(width)


  const isDesktop = width >= 992

  useEffect(()=>{
    setWidth(window.innerWidth)
  },[])

  console.log(isDesktop)
  return <>

    {isDesktop ? <div className="certificates-top">
        <div className="certificates-top-left"/>
        <div className="certificates-top-center ">
          <h5 className="certificates-top-header font-size-64 font-weight-bolder border-right-green">Certyfikaty</h5>
        </div>

        <div className="certificates-top-right"/>
      </div> : <div className="cwi-container-mobile-header font-size-32">
        Certyfikaty
      </div>}

    <div className="certificates-bottom">
      <div className="certificates-bottom-left"/>
      <Row className="certificates-bottom-center">
        <img src="/images/certificates/3.jpg" className="w-100 h-100"/>
      </Row>
      <div className="certificates-bottom-right"/>
    </div>

    <div className="certificates-bottom">
      <div className="certificates-bottom-left"/>
      <Row className="certificates-bottom-center">
        <img src="/images/certificates/4.jpg" className="w-100 h-100"/>
      </Row>
      <div className="certificates-bottom-right"/>
    </div>

    <div className="certificates-bottom">
      <div className="certificates-bottom-left"/>
      <Row className="certificates-bottom-center">
        <img src="/images/certificates/5.jpg" className="w-100 h-100"/>
      </Row>
      <div className="certificates-bottom-right"/>
    </div>

  </>
}


