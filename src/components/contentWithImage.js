import React from 'react'
import {Col, Row} from "reactstrap";
import Button from "./button";
import {CopyToClipboard} from "react-copy-to-clipboard/lib/Component";

export const ContentWithImage = ({
                                   revert,
                                   imgSrc = "",
                                   headerText,
                                   imgStyle = {},
                                   description,
                                   buttonText,
                                   cwiStyle,
                                   onClick,
                                   id,
                                   key,
                                   contactButtons
                                 }) => {

  console.log(headerText)
  return <>
    <div id={headerText} key={key} className="cwi-container" style={cwiStyle}>
      <div className={`cwi ${revert && 'flex-row-reverse'}`}>
        <div className="cwi-left cwi-grey"/>
        <Row className={`cwi-center ${revert && 'flex-row-reverse'}`}>
          <Col sm={4} className="cwi-center-col">
            <img src={imgSrc} style={imgStyle} className="cwi-center-about1"/>
          </Col>
          <Col sm={8} className={`p-0 m-0 ${!buttonText && 'cwi-grey'}`}>
            <div className={`cwi-grey p-4 ${revert ? 'border-left-green' : 'border-right-green'}`}>
              <h5 className="font-size-64 font-weight-bolder cwi-center-header">{headerText}</h5>
              <p className="font-size-16">{description}</p>

              {contactButtons && <div className="popup-content-row-buttons">
                <div className="popup-content-row-buttons-element">
                  <div>
                    <img width="23px" src="/images/icons/insta.svg"/> @surowo.mi
                  </div>
                  <Button onClick={ () => window.location.href = `https://www.instagram.com/surowo.mi/`} buttonText="Napisz!"/>
                </div>
                <div className="popup-content-row-buttons-element">
                  <div>
                    <img width="23px" src="/images/icons/mail-icon.svg"/> surowomi@gmail.com
                  </div>
                  <CopyToClipboard text="surowomi@gmail.com">
                    <Button buttonText="Kopiuj"/>
                  </CopyToClipboard>
                </div>
              </div>}

            </div>
            {buttonText && <div className={`m-4  ${revert && "text-right"}`}>
              <Button buttonText={buttonText} onClick={() => {
                console.log(id)
                onClick(id)
              }}/>
            </div>}
          </Col>
        </Row>
        <div className="cwi-right"/>
      </div>
    </div>
    <div class="cwi-container-mobile">
      <div class="cwi-container-mobile-header font-size-32">
        {headerText}
      </div>
      <div className="cwi-container-mobile-paragraph">
        {description}
      </div>
      <div className="cwi-container-mobile-image">
        <img className=" w-100 h-100" src={imgSrc}/>
      </div>
    </div>
  </>
}