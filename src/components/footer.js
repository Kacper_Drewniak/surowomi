import React from 'react'
import {AiOutlineInstagram} from "react-icons/ai";


export const Footer = () => {


  return <div className="footer">
    <div className="footer-content">
      <div>© Copyright - surowo.mi</div>
      <div className="font-size-32">
        <a href="https://www.instagram.com/surowo.mi/"><AiOutlineInstagram/></a>
      </div>
      <div className="footer-content-comtru">
        <a href="https://comtru.art/">Projekt i realizacja: <span>Comtru</span>.art</a>
      </div>
    </div>
  </div>
}