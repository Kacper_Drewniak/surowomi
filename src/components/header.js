import React from 'react'
import {Link} from "gatsby"

export const Header = ({text}) => {

  return <div className="header">
    <h1 className="header-text ">{text}</h1>
    <h4 className="header-paragraph">{text}</h4>
  </div>
}
