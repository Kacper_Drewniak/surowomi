import React from 'react'
import {Row, Col} from "reactstrap";

export const Hero = () => {

  return <div className="hero">
    <Row className="hero-content">
      <Col className="hero-content-col ">
        <img className="hero-content-col-image" src="images/hero_avatar.jpg"/>
      </Col>
      <Col className="hero-content-col">
        <h1 className="font-size-32 font-weight-bolder">Surowo.mi</h1>
        <h2 className="font-size-64 font-weight-bolder">Alicja Maziarz</h2>
        <p className="hero-content-col-paragraph font-size-16">
          <p>Żyj tu i teraz</p>
          <p>Słuchaj swojej intuicji</p>
          <p>Życie to energia i jej wymiana. Sztuką jest korzystać z niej dawać</p>
          <p>i brać, chłonąć i płynąć.</p>
          <br/>
          <p>
            Cześć jestem <strong>Alicja</strong>
          </p>
          <p>Miło, że jesteś , Rozgość się</p>
          <p>
            <br/>
            Zabiorę Cię w podróż wgłąb siebie. Będzie to najpiękniejsza podróż Twojego życia. Pozwól sobie na nią.
            Spakuj to walizki swoje serce bo to podróż do własnego wnętrza. Do serca gdzie nie ma lęku, do bycia w
            stanie -JAM JEST.
          </p>
          <br/><br/>
          <p>
            <span style={{fontStyle: 'italic'}}>
              „Jeśli doprowadzisz do porządku własne wnętrze, to, co na zewnątrz samo ułoży się we właściwy sposób.”
            </span>
            Eckhart Tolle
          </p>
        </p>
      </Col>
    </Row>
  </div>
}