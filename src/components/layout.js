import React from 'react'
import {Container} from "reactstrap";
import {Navbar} from "./navbar";
import {Footer} from "./footer"
import Seo from "./seo";

const Layout = ({children, title, link}) => <Container fluid>
  <Navbar link={link}/>
  <Seo title={title}/>
  {children}
  <Footer/>
</Container>

export default Layout
