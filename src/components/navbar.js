import React, {useState} from 'react'
import {Link} from "gatsby"
import {Twirl, Twirl as Hamburger} from 'hamburger-react'

export const Navbar = ({link}) => {

  const [isOpen, setOpen] = useState(false)


  console.log(isOpen)

  return <>
    <div className="navbar">
      <div className="navbar-content">
        <div className="navbar-content-list">
          <Link className="navbar-content-list-logo" to="">
            <img src="/images/logo.svg"/>
          </Link>
        </div>
        <div className="navbar-content-list">
          <Link className={`navbar-content-list-link ${link === "index" && "active"}`} to="/">Strona głowna</Link>
          <Link className={`navbar-content-list-link ${link === "about" && "active"}`} to="/about">O mnie</Link>
          <Link className={`navbar-content-list-link ${link === "offer" && "active"}`} to="/offer">Oferta</Link>
          <Link className={`navbar-content-list-link ${link === "recipes" && "active"}`} to="/recipes">Przepisy</Link>
          <Link className={`navbar-content-list-link ${link === "contact" && "active"}`} to="/contact">Kontakt</Link>
        </div>
      </div>
    </div>
    <div className="navbar-mobile">
      <div className="navbar-mobile-content">
        <Link className="navbar-mobile-list-logo" to="">
          <img src="/images/logo.svg"/>
        </Link>
        <Twirl toggled={isOpen} toggle={setOpen} size={20}/>
        <div className={`navbar-mobile-menu ${isOpen && "active"}`}>
          <div className="navbar-mobile-menu-linkers">
            <Link className={`navbar-content-list-link ${link === "index" && "active"}`} to="/">Strona głowna</Link>
            <Link className={`navbar-content-list-link ${link === "about" && "active"}`} to="/about">O mnie</Link>
            <Link className={`navbar-content-list-link ${link === "offer" && "active"}`} to="/offer">Oferta</Link>
            <Link className={`navbar-content-list-link ${link === "recipes" && "active"}`} to="/recipes">Przepisy</Link>
            <Link className={`navbar-content-list-link ${link === "contact" && "active"}`} to="/contact">Kontakt</Link>
          </div>
        </div>
      </div>
    </div>
  </>
}
