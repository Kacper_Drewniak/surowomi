import React from 'react'
import {Card, CardGroup, CardImg, CardTitle, Col, Row} from "reactstrap";
import Button from "./button";
import loadable from '@loadable/component'
import '@brainhubeu/react-carousel/lib/style.css';

const Carousel = loadable(() => import('@brainhubeu/react-carousel'))
export const Offer = () => {

  const Tile = ({src, text}) => <Card className="offer-content-cards-item">
    <CardImg
      onClick={()=>window.location.href=`/offer`}
      alt="Card image cap"
      className="offer-content-cards-item-image"
      src={src}
      width="100%"
    />
    <CardTitle className="offer-content-cards-item-title"
    >
      {text}
    </CardTitle>
  </Card>

  const tiles = [
    {
      src: 'images/offer/forest.jpg',
      text: 'Theta healing'
    },
    {
      src: 'images/offer/smile.jpg',
      text: 'Irydologia oczu'
    },
    {
      src: 'images/offer/yoga.jpg',
      text: 'Kundalini joga'
    },
    {
      src: 'images/offer/tree.jpg',
      text: 'Boska doskonałość'
    },
    {
      src: 'images/offer/pregnacy.jpg',
      text: 'Cud stworzenia'
    },
    {
      src: 'images/offer/fruits.jpg',
      text: 'E-book'
    }]


  return <>
    <div className="offer">
      <div className="offer-content">
        <Row className="offer-content-top">
          <Col xs="auto">
            <h4 className="font-size-32 font-weight-bolder">Oferta</h4>
          </Col>
          <Col xs="auto">
            <Button buttonText="Zobacz ofertę" onClick={()=>window.location.href="/offer"}/>
          </Col>
        </Row>
        <CardGroup className="offer-content-cards">
          {tiles.map(({src, text}) => <Tile src={src} text={text}/>)}
        </CardGroup>
      </div>
    </div>

    <div className="offer-mobile">
      <Carousel plugins={['arrows']}>
        {tiles.map(({src, text}) => <Tile src={src} text={text}/>)}

      </Carousel>
    </div>

  </>
}


