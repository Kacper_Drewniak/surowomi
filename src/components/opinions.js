import React from 'react'
import {Card, CardText, CardTitle, Col, Row} from "reactstrap";
import Button from "./button";


const OpinionElement = ({title, serviceType, sourceType, text}) => <Col sm="6" className="opinion-content-col ">
  <Card body className="opinion-content-bottom-card border-bottom-green">
    <CardTitle className="font-size-32 font-weight-bolder font-color-green">
      {title}
    </CardTitle>
    <CardText className="opinion-content-bottom-cardtext-top">
      <span className="font-weight-bolder"> Typ usługi: </span>
      {serviceType}
    </CardText>
    <CardText className="opinion-content-bottom-cardtext">
      <span className="font-weight-bolder"> Źródło: </span>
      {sourceType}
    </CardText>
    <CardText className="opinion-content-bottom-cardtext">

      {text}

    </CardText>
  </Card>
</Col>

export const Opinions = () => {

  const opinions = [
    {
      title: 'Agnieszka',
      serviceType: 'proces "Jestem boską doskonałością"',
      sourceType: 'Facebook - grupa "Jestem boską doskonałością',
      text: `Kochane, zbieram się długo, by zamknąć w kilka zdań to, co wodospadem przelewa się przeze mnie. Jest to
              niemożliwe zadanie - to, czym się dzielicie też "zalało mnie wodospadem" - Wasze zaangażowanie, energia,
              wiedza, intuicja, humor, delikatność, pasja, dbałość o szczegóły, estetyka, cierpliwość, wsparcie i wiele
              innych płynęły ze szczerego serca - wszystkie dotknęły mojego na każdym poziomie. Dzięki wam dałam sobie
              zielone światło do przemiany już na zawsze <3 Jestem ogromnie wdzięczna :)`
    },
    {
      title: 'Emilia',
      serviceType: 'Irydologia oczu',
      sourceType: 'Wiadomość przesłana mailem',
      text: `Bardzo Pani dziękuje! Analiza jest bardzo szczegółowa - jestem pod wrażeniem, że tyle można wyczytać z samej tęczówki! :) Osłabienie nerki podejrzewałam już od dłuższego czasu, ale teraz już wiem, gdzie dokładnie jest problem i dowiedziałam się dużo nowych i ważnych rzeczy, które na pewno pomogą mi w procesie uzdrawiania. :)`
    },
    {
      title: 'Dominika',
      serviceType: 'Irydologia oczu',
      sourceType: 'Wiadomość przesłana mailem',
      text: `Jesteś naprawdę kochana, polecam Cię moim znajomym, bo to, co napisałaś, naprawdę mi się zgadza w 100% bez chodzenia po szpitalach, czego nie znoszę i co najważniejsze żadnych leków!!`
    },
    {
      title: 'Gosia',
      serviceType: 'proces "Jestem boską doskonałością"',
      sourceType: 'Facebook - grupa "Jestem boską doskonałością"',
      text: `To był wspaniały czas. Alicja i Magda stworzyły cudowny program, dzięki któremu oprócz oczyszczenia i uzdrowienia organizmu na poziomie fizycznym poczułam niesamowitą kosmiczną energię! Pracowałyśmy nad nią podczas wielu warsztatów, medytacji, sesji Theta healing i Soul body Fusion <3 Były łzy i momenty euforii. Praktykowałyśmy o świcie Jogę Kundalini, która zostanie z mną na dłużej <3 Czułam się zaopiekowana. W słabszych chwilach wpierałyśmy się wszystkie na bieżąco. Nie było pytań bez odpowiedzi. Dziewczyny - jesteście mega inspiracją. Z chęcią powtórzyłabym ten proces za jakiś czas. Transformacja trwa. Dziękuje z całego serca <3`
    }

  ]


  return <div className="opinion">
    <div className="opinion-content">
      <Row className="opinion-content-top">
        <Col xs="auto">
          <h4 className="font-size-32 font-weight-bolder">Opinie</h4>
        </Col>
      </Row>
      <Row className="opinion-content-bottom">
        {opinions.map(opinion => <OpinionElement  {...opinion}/>)}
      </Row>
    </div>
  </div>

}