import React from 'react'
import {Col, Row} from "reactstrap";
import Button from "./button";
import {CopyToClipboard} from "react-copy-to-clipboard/lib/Component";

export const Popup = ({popup, handleClosePopup}) => {
  console.log(popup.id)
console.log(popup.customMail)
  const ifSession = popup.id == 2 || popup.id == 3 || popup.id == 4 || popup.id == 5
  return <div className="popup active">
    <div className="popup-content border-bottom-green">
      <div className="popup-content-navi"><img onClick={handleClosePopup}
                                               className="popup-content-xicon" src="/images/icons/x-icon.svg"/></div>
      <Row className='popup-content-row'>
        <Col className="popup-content-row-img" sm={4}>
          <img className="h-100 w-100" src={popup.imgSrc}/>
        </Col>
        <Col sm={12} lg={8} className="popup-content-row-content">
          <h1 className="font-weight-bolder font-size-64">
            {popup.headerText}
          </h1>
          <h3 className="font-weight-bolder font-size-32">
          </h3>
          <p>
            {popup.description}
          </p>
          {
            ifSession && <div className="popup-content-row-buttons">
              <div className="popup-content-row-buttons-element">
                <div>
                  <img width="23px" src="/images/icons/mail-icon.svg"/> surowomi@gmail.com
                </div>
                <CopyToClipboard text="surowomi@gmail.com">
                  <Button buttonText="Kopiuj"/>
                </CopyToClipboard>
              </div>
              <div className="popup-content-row-buttons-element">
                <div>
                  <img width="23px" src="/images/icons/mail-icon.svg"/> Tytuł: Theta healing - {popup.headerText}
                </div>
                <CopyToClipboard text="Theta healing">
                  <Button buttonText="Kopiuj"/>
                </CopyToClipboard>
              </div>
            </div>
          }
          {
            popup.id === 10 && <div className="popup-content-row-buttons">
              <div className="popup-content-row-buttons-element">
                <div>
                  <img width="23px" src="/images/icons/mail-icon.svg"/> surowomi@gmail.com
                </div>
                <CopyToClipboard text="surowomi@gmail.com">
                  <Button buttonText="Kopiuj"/>
                </CopyToClipboard>
              </div>

            </div>
          }
          {
            popup.id == 6 && <div className="popup-content-row-buttons">
              <div className="popup-content-row-buttons-element">
                <div>
                  <img width="23px" src="/images/icons/mail-icon.svg"/> surowomi@gmail.com
                </div>
                <CopyToClipboard text="surowomi@gmail.com">
                  <Button buttonText="Kopiuj"/>
                </CopyToClipboard>
              </div>

            </div>
          }
          {
            popup.id == 7 && <div className="popup-content-row-buttons">
              <div className="popup-content-row-buttons-element">
                <div>
                  <img width="23px" src="/images/icons/fb.svg"/> Dołącz
                </div>
                <Button onClick={ () => window.location.href = `https://www.facebook.com/`}  buttonText="Dołącz"/>
              </div>
            </div>
          }
          {
            popup.id == 8 && <div className="popup-content-row-buttons">
              <div className="popup-content-row-buttons-element">
                <div>
                  <img width="23px" src="/images/icons/insta.svg"/> @surowo.mi
                </div>
                <Button onClick={ () => window.location.href = `https://www.instagram.com/surowo.mi/`} buttonText="Napisz!"/>
              </div>
              <div className="popup-content-row-buttons-element">
                <div>
                  <img width="23px" src="/images/icons/mail-icon.svg"/> <>{popup.customMail ? popup.customMail : 'surowomi@gmail.com'}</>
                </div>
                <CopyToClipboard text="surowomi@gmail.com">
                  <Button buttonText="Kopiuj"/>
                </CopyToClipboard>
              </div>
            </div>
          }
          {
            popup.id == 9 && <div className="popup-content-row-buttons">
              <div className="popup-content-row-buttons-element">
                <div>
                  <img width="23px" src="/images/icons/mail-icon.svg"/> Kopiuj
                </div>
                <Button onClick={ () => window.location.href = `https://www.instagram.com/surowo.mi/`} buttonText="Napisz!"/>
              </div>
            </div>
          }
        </Col>
      </Row>
    </div>
  </div>
}