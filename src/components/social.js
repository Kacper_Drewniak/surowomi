import React from 'react'
import InstagramIcon from '@mui/icons-material/Instagram';

import Button from "./button";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

export const Social = ({text}) => {

  return <div className="social">
    <div className="social-content">
      <h1 className="font-size-32 font-weight-bolder d-flex align-items-center">
        <InstagramIcon style={{width: '40px', height: '40px'}}/>
        Instagram
      </h1>
      <div>
        <span className="social-content-all">Wszystko w jednym miejscu </span>

        <a href="https://www.instagram.com/surowo.mi/"  style={{textDecoration:'none'}} className="mt-2 social-content-button font-size-16 font-color-white font-weight-bolder">Zaobserwuj mnie na Instagramie</a>
      </div>
    </div>
  </div>
}


