import React from "react"
import Layout from "../components/layout"
import "./../styles/index.scss"
import {AboutMe} from "../components/aboutme";
import {Header} from "../components/header";
import {Certificates} from "../components/certificates";

const About = () => (
  <Layout title="O mnie" link="about">
    <Header text="O mnie"/>
    <AboutMe/>
    <Certificates/>
  </Layout>
)

export default About