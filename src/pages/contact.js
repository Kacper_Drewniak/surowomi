import React from "react";
import Layout from "../components/layout";
import {Header} from "../components/header";
import {ContentWithImage} from "../components/contentWithImage";
import Button from "../components/button";
import {CopyToClipboard} from "react-copy-to-clipboard/lib/Component";


const Contact = () => {

  return <Layout title="Kontakt" link="contact">
    <Header text="Kontakt"/>
    <ContentWithImage
      contactButtons={true}
      imgSrc="/images/contact.jpg"
      headerText="Chcesz zobaczyć więcej?"
      description={<>

        Tylko ja i moja przestrzeń. Dzielę się tym co najlepsze
        obserwuj mnie na <a style={{color:"black"}} href="https://www.instagram.com/surowo.mi/">instagramie</a>
        <br/> <br/>
        Skontaktuj się ze mną
        <br/> <br/>

        Chcesz zapisać się na sesje, irydologię lub warsztaty?
        <br/> <br/>
        A może rozpoczniemy współpracę?</>}
    />
    <div className="popup-content-row-buttons d-lg-none d-sm-block">
      <div className="popup-content-row-buttons-element">
        <div>
          <img height="23px"  src="/images/icons/insta.svg"/> @surowo.mi
        </div>
        <Button  onClick={ () => window.location.href = `https://www.instagram.com/surowo.mi/`} buttonText="Napisz!"/>
      </div>
      <div className="popup-content-row-buttons-element">
        <div>
          <img height="23px" src="/images/icons/mail-icon.svg"/> surowomi@gmail.com
        </div>
        <CopyToClipboard text="surowomi@gmail.com">
        <Button buttonText="Kopiuj"/>
        </CopyToClipboard>
      </div>
    </div>
  </Layout>
}


export default Contact