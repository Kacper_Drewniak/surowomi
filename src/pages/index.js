import React from "react"
import Layout from "../components/layout"
import "./../styles/index.scss"
import {Hero} from "../components/hero";
import {Social} from "../components/social";
import {Offer} from "../components/offer"
import {Opinions} from "../components/opinions";

const IndexPage = () => (
  <Layout title="Główna" link="index">
    <Hero/>
    <Social/>
    <Offer/>
    <Opinions/>
  </Layout>
)

export default IndexPage
