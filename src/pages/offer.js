import React, {useState} from "react"
import Layout from "../components/layout"
import "./../styles/index.scss"
import {Header} from "../components/header";
import {ContentWithImage} from "../components/contentWithImage";
import {Session} from "../components/Session";
import {Popup} from "../components/popup";
import {Col} from "reactstrap";

const Offer = () => {

  const [popup, setPopup] = useState(null)

  const firstOffer = {
    id: 1,
    cwiStyle: {margin: '13rem 0 5rem 0'},
    imgStyle: {height: '220%'},
    imgSrc: '/images/thetahealing.jpg',
    headerText: "Theta healing",
    description: <>Co to jest stan <strong>Theta</strong>? To stan bardzo głębokiego wyciszenia i relaksacji w którym Nasze fale
      mózgowe zwalniają do częstotliwości 4-7 Hz. dzięki temu możemy <strong>dotrzeć</strong> do tej części umysłu, która położona jest
      pomiędzy naszą świadomością i podświadomością . Tam właśnie znajdują się nasze <strong>wspomnienia, przekonania, emocje
      i doznania.</strong> Podczas takiej sesji mamy możliwość zmiany danego wzorca zachowań na poziomie fizycznym, mentalnym
      lub metafizycznym.
      <br/>
      <br/>
      Jest to proces medytacyjny o mocy uzdrawiania fizycznego, psychicznego i duchowego.</>,
  }

  const offers = [{
    link:"Theta",
    id: 6,
    headerText: "Irydologia oczu",
    description: <>
      Czyli bezinwazyjne badanie tęczówki oka.<br/><br/> Irydologia podchodzi do Naszego organizmu <strong>holistycznie</strong>. Traktuje człowieka jako
      psychofizyczną całość, co daje możliwość spojrzenia na organizm bez dzielenia go na chory żołądek, serce czy
      nerwy.
      <br/>
      <br/>
      To właśnie w oku umiejscowione są zakończenia połączeń nerwowych powiązanych z każdym narządem wewnętrznym i każdą
      częścią naszego ciała dlatego wygląd tęczówki zdradza Nasz obecny <strong>stan zdrowia</strong>.
      <br/>
      <br/>
      Na podstawie obserwacji tęczówki jestem w stanie określić predyspozycje genetyczne do pewnych
      schorzeń <br/>i
      osłabienia narządów, rozpoznać stan ostry, podostry, przewlekły i degeneracyjny w organizmie, określić miejsce
      jego występowania, stwierdzić obecność toksyn, nieprawidłowego oczyszczania organizmu i oznaki stresu.
      <br/><br/>
      Czas oczekiwania: <strong>3-4 dni robocze</strong>
      <br/>
      Cena za sesję: <strong>150 zł</strong>
      <br/><br/></>,
    imgSrc: '/images/offer/smile.jpg'
  }, {
    link:"Irydologia",
    id: 7,
    headerText: "Kundalini Joga",
    description: <>
      Przekonaj się jak działa <strong>kundalini joga</strong>. Jeśli pragniesz odkryć swój potencjał, dotrzeć do swojej wewnętrznej siły
      i mocy, odkryć i poczuć swoje wewnętrzne piękno oraz aby Twoje życie było przepełnione radością i pasją. Jesteś we
      właściwym miejscu. <strong>kundalini joga</strong> właśnie tego uczy, jest drogą wychodzenia z ograniczeń umysłu, doświadczania
      magii życia z poziomu serca, łączy Nas z wewnętrznym mistrzem.
      <br/>
      <br/>
      Co daje Nam praktyka <strong>KUNDALINI Jogi:</strong>
      <br/>
      <br/>
      – głęboką transformacje na poziomie fizycznym, mentalnym i duchowym, <br/>
      - stymuluje organizm do samouzdrawiania i silnego oczyszczania <br/>
      – poszerza świadomość <br/>
      - silnie rozbudza intuicję, twórczy i mentalny potencjał <br/>
      - pomaga odnaleźć naszą ścieżkę życiową <br/>– zaczyna się PASJA ŻYCIA <br/>
      <br/>
      <br/>
      Przekonaj się jak bardzo silne są techniki <strong>Kundalini Jogi</strong>! Daj sobie szansę i mniej odwagę,<br/> a zobaczysz, jak
      zmieni się Twoje życie.   <br/>
      <br/><br/>
      Praktyki odbywają się online na zamkniętej grupie na Facebooku.   <br/>

      Zapraszam po więcej szczegółów na mój profil na instagramie lub do kontaktu meilowego   <br/>

      <strong>jestemboskadoskonaloscia@gmail.com</strong>
    </>,
    imgSrc: '/images/offer/kundalini.jpg',
    buttonName:"Dołącz"
  },
    {
      buttonName:"Zapisz się",
      link:"Kundalini",
      id: 8,
      headerText: "Jestem Boską Doskonałością",
      description: <>
        To wspaniała <strong>podróż</strong> w głąb siebie. Podczas procesu jesteśmy na poście sokowym, który daje Nam przestrzeń aby tak
        naprawdę spotkać się ze sobą i zajrzeć w głąb swojego serca. Projekt ten tworzę wraz z Madzią Burczyk-Gwóźdź.
        <br/>
        <br/>

        Podczas spotkań na żywo odkrywamy moc jaką w sobie mamy, poznajemy Nasze ciało i umysł, poszerzamy Naszą
        świadomość i wydobywamy Nasz boski potencjał. Narzędzia jakie Nam w tym pomagają to między innymi:<br/> praca z
        oddechem, medytacje Theta healing, medytacje soul body fusion, joga kundalini i wiele innych.
        <br/>
        <br/>
        W ciągu tych czternastu dni:
        <br/>
        <br/>
        <strong>pokochasz </strong>siebie na nowo i zaczniesz <strong>żyć </strong>pełnią życia
        oczyścisz swoje ciało i umysł
        odzyskasz wiarę w siebie i swoje możliwości
        zmienisz nawyki żywieniowe
        <strong> pożegnasz</strong> kompulsywne jedzenie
       <strong> pożegnasz</strong> ograniczające Cię przekonania
        i wiele więcej

      </>,
      imgSrc: '/images/offer/hapiness.jpg',
      cwiStyle: {margin: "20rem 0"},
      customMail:'jestemboskadoskonaloscia@gmail.com'
    },

    {
      link:"Yogawciazy",
      id: 10,
      buttonName:"Zapisz się",
      headerText: "Cud stworzenia ",
      description: <>

        Często w dzisiejszym pędzie życia zaczynamy o siebie dbać , opiekować się sobą , być uważną gdy dowiemy się, że jesteśmy w ciąży. Zastanawiałaś się czemu nie robimy tego wcześniej? Dlaczego świadomie nie przygotowujemy się do tej najpiękniejszej roli w Naszym życiu? Dlaczego Nasi partnerzy tego nie robią? Dlaczego cała odpowiedzialność spada na Nas KOBIETY? <br/> <br/>

        <strong>Co Cię czeka podczas tych sześciu tygodni</strong> ?<br/><br/>
        - trzy razy w tygodniu na żywo praktyki kundalini jogi i medytacje na których będziemy oczyszczać się ze wszystkich energii swoich byłych partnerów ( czas trwania praktyki 1,5 godziny)
        <br/>

        - osiem spotkań na platformie zoom ( czas trwania spotkania 2-3 godziny) na których będziemy:
        <br/>
        - uzdrawiać życie płodowe
        <br/>
        - pracować z wewnętrznym dzieckiem
        <br/>
        - pracować z przekonaniami – rozpoznawać je i transformować w głębokim procesie uzdrawiania theta healing
        <br/>
        - wpajać miłość bezwarunkową
        <br/>
        - zrobimy proces radykalnego wybaczania
        <br/>
        - porozmawiamy o tym jak Nasze emocje, zachowania, myśli, otoczenie wpływa na Nasze wnętrze i nauczymy się słuchać Naszego ciała
        <br/>
        -porozmawiamy o energii seksualnej czym ona jest, co wnosi do Naszego życia i jak z niej korzystać aby przyniosła do Naszego życia pasję , radość i obfitość.
        <br/>
        - porozmawiamy o najlepszym dla naszego gatunku odżywianiu
        <br/>
        - oczyszczaniu organizmu aby był gotowy na przyjęcie CUDU stworzenia i zapewnił najlepsze warunki do rozwoju
        <br/>
        - i wiele wiele innych WAŻNYCH tematów
        <br/>
<br/>
        Proces zaczyna się <strong>07-02-2022</strong> a kończy <strong>18-03-2022</strong> w pełnię księżyca.<br/> To 40 dni które pięknie przygotuje Cię do roli MAMY i taty :)
        <br/><strong>Inwestycja : 888 zł</strong><br/>

         surowomi@gmail.com

      </>,
      imgSrc: '/images/offer/pregnacy.jpg'
    },  {
      link:"Boska",
      imgStyle: {objectFit:'contain'},
      id: 9,
      headerText: "E-book",
      description: <>
        E-book ten to pigułka wiedzy o postach sokowych. Został wydany w moje urodziny i jest mi bardzo bliski, gdyż
        została w nim zawarta cała wiedza i osobiste doświadczenie o tym jak przygotować się do postu sokowego, jak go
        przejść , jak poprawnie z niego wyjść i wiele wiele innych cennych wskazówek.
        Zapraszam serdecznie do zakupu.
        <br/>
        <br/>
        Zamówienia proszę kierować na adres meilowy : jestemboskadoskonaloscia@gmail.com
        <br/>
        <br/>
        Koszt ebooka : 44 zł
      </>,
      imgSrc: '/images/offer/fruits.jpg',
      buttonName: "Kup e-booka"
    },]


  const sessionsText = [{
    id: 2,
    headerText: "Praca z przekonaniami i uczuciami",
    imgSrc: '/images/thetahealing.jpg',
    description: <>
      <span style={{fontStyle: 'italic'}}>„Nie wierzysz w to co widzisz, lecz widzisz to, w co już uwierzyłeś” - Wayne Dyer</span><br/>
      Każdy chce, aby jego życie było znaczące i pełne radości, jednak nie zawsze wiemy , jak to osiągnąć. Każdy z Nas
      nieustannie kształtuje swoje <strong>przekonania</strong>, przejmując je od innych albo tworząc samemu. Jednak czy są one prawdziwe
      i jak wpływają na Nas, dowiadujemy się później, ponieważ gdy my kształtujemy swoje poglądy, one kształtują nasze
      życie.
      <br/>
      <br/>
      <strong>Przekonania</strong> są tym, w co głęboko wierzysz, z czym się identyfikujesz, na czym budujesz poczucie własnej wartości i
      tożsamość. Mogą dodawać nam skrzydeł, a mogą powodować psychiczny dyskomfort czy nawet cierpienie. Postrzegasz
      otaczający Cię świat przez pryzmat własnych przekonań, uprzedzeń oraz nastawienia. Wszelkie Twoje działania
      stanowią odzwierciedlenie tego, co myślisz o sobie i innych.
      <br/>
      <br/>
      Czy uważasz, że : „ emocji się nie okazuje”, „ jestem słaby” , „ ludzie ciągle mnie krytykują ” . Zacznij od
      zmiany swojego wewnętrznego stanu.
      Zapraszam Cię na <strong>podróż w głąb siebie</strong>.

      <br/>
      <br/>
      Czas trwania sesji: <strong>60-90 min</strong>
      <br/>
      Cena za sesję: <strong>250 zł</strong>
      <br/><br/>
    </>
  }, {
    id: 3,
    headerText: "Miłość bezwarunkowa",
    imgSrc: '/images/thetahealing.jpg',
    description: <>
      “<strong>Miłość bezwarunkowa</strong> istnieje w każdym z nas. Stanowi naszą najgłębszą część. Nie jest aktywnym uczuciem, a raczej
      stanem. To nie <strong>“kocham Cię”</strong> z tego lub innego powodu ani “<strong>kocham Cię</strong>, jeśli Ty mnie kochasz”.
      To miłość bez powodu, miłość bez celu. To po prostu siedzenie w miłości, miłość, która obejmuje krzesło i pokój
      oraz przenika wszystko wokół. Umysł myślący jest wygaszony miłością.”
      <br/>
      <br/>
      -Guru Ram Dass-
      <br/>
      <br/>
      <strong>Miłość bezwarunkowa</strong> to najczystsze i najbardziej szlachetne uczucie na całym świecie. To <strong>kochanie</strong> kogoś nie
      oczekując niczego w zamian, <strong> kochanie</strong> kogoś każdą cząstką swojego istnienia. Oznacza <strong>kochanie</strong> kogoś za to, jaki
      jest, bez względu na to, co robi lub mówi, nieważne, czy jest z Tobą, czy nie.
      <br/>
      <br/>
      Podczas sesji medytacyjnej prowadzonej w przestrzeni serca, uwolnimy nagromadzone energie w postaci traum i
      trudnych emocji, zaopiekujemy się <strong>wewnętrznym dzieckiem</strong> i popracujemy z <strong>radykalnym wybaczaniem</strong>.
      Jest to prawdziwa podróż w głąb siebie, skupiona na czuciu od wewnątrz, której rezultatem jest uwolnienie blokad,
      zmiana stosunku do siebie, innych oraz otaczającej nas rzeczywistości.
      <br/>
      <br/>
      <br/>
      Czas trwania sesji  <strong>60-90 min</strong>
      <br/>
      Cena za sesję: <strong>200 zł</strong>
      <br/><br/>
    </>
  }, {
    id: 4,
    headerText: "Oczyszczanie przestrzeni",
    imgSrc: '/images/thetahealing.jpg',
    description: <>
      Czujesz, że<strong> złe</strong> wibracje utrzymują się w <strong>Twojej przestrzeni?</strong> Toksyczna energia z kłótni lub chorób? Twój dom,
      firma czy miejsce pracy jest czyste i zorganizowane, ale nadal ma <strong>ciężką energię</strong>?
      <br/>
      <br/>
      Podczas sesji <strong>oczyścimy</strong> Twoja przestrzeń i wpoimy nowe uczucia tak aby stała się <strong>lekka i świeża</strong> niczym świątynia
      spokoju, miłości i twórczości.
      <br/>
      <br/>
      Czas trwania sesji: <strong>45 min</strong>
      <br/>
      Cena za sesję: <strong>100 zł</strong>
      <br/><br/>
    </>
  }, {
    id: 5,
    headerText: "Uzdrawianie życia płodowego i wewnętrznego dziecka",
    imgSrc: '/images/thetahealing.jpg',
    description: <>
      Od momentu poczęcia jesteśmy <strong>świadomi</strong> wszystkiego ,co się wokół Nas dzieje. Uczucia, emocje i przekonania matki
      są często przenoszone na dziecko w łonie. Bycie niechcianym, alkohol, narkotyki, poprzednie aborcje matki
      wpływają
      na Nasz rozwój fizyczny i umysłowy.
      <br/>
      <br/>
      To wspaniały uzdrawiający proces. Można go wykonać dla siebie, dla swoich dzieci czy rodziców.
      <br/>
      <br/>
      Dzięki wysłaniu <strong>miłości</strong> do dziecka w łonie przeprogramujesz swoją podświadomość z miejsca bezwarunkowej <strong>miłości</strong>.
      <br/>
      <br/>
      Czas trwania sesji: <strong>45 min</strong>
      <br/>
      Cena za sesję: <strong>100 zł</strong>
      <br/><br/>
    </>
  }
  ]

  console.log(sessionsText)

  const combineArray = [
    firstOffer, ...sessionsText, ...offers
  ]

  console.log(combineArray)

  const handleOpenPopup = (id) => {
    console.log(offers)
    console.log(id)
    setPopup(combineArray.find(el => el.id === id))
  }

  const handleClosePopup = () => {
    setPopup(null)
  }

  return <Layout title="Oferta" link="offer">
    <Header text="Oferta"/>
    {popup && <Popup popup={popup} handleClosePopup={handleClosePopup}/>}
    <ContentWithImage
      {...firstOffer}
    />
    <Session sessionsText={sessionsText} onClick={handleOpenPopup}/>
    {offers.map(({headerText, description, imgSrc, cwiStyle, id,buttonName,link}, index) => <ContentWithImage headerText={headerText}
                                                                                              description={description}
                                                                                              id={id}
                                                                                                              link={link}
                                                                                              imgSrc={imgSrc}
                                                                                              cwiStyle={cwiStyle}
                                                                                              revert={index % 2}
                                                                                              buttonText={buttonName ? buttonName : "Umów sie"}
                                                                                              onClick={handleOpenPopup}
    />)}


  </Layout>
}


export default Offer