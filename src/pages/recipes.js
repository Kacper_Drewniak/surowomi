import React from "react";
import Layout from "../components/layout";
import {Hero} from "../components/hero";
import {Header} from "../components/header";
import {ContentWithImage} from "../components/contentWithImage";


const Recipes = () => {

  const recipes = [{
    headerText: "SUROWE sushi :)",
    description: <>
      Kochani nie ma nic prostszego a zarazem pysznego i zdrowego w przygotowaniu :)
      Uwielbiam <strong>surowe sushi</strong> mogłabym się nim zajadać codziennie, no może do drugi dzień :):)
      Wykonanie jest banalnie proste.
      <br/><br/>
      <strong>POTRZEBUJEMY:</strong>
      <br/><br/>
      <strong> Listki nori</strong>, albo można użyć liście <strong>sałaty</strong>. Ja osobiście bardzo lubię smak nori , ale wiem, że nie każdemu smakują
      dlatego alternatywa sałaty jest super. Kwestia kreacji i zabawy w kuchni :)
      <strong> Ryż </strong>: oczywiście surowy. Ja robię go albo z surowego selera, kalafiora lub brokułu. Rozdrabniam w blenderze z
      dodatkiem ziołowego pieprzu i soli kłodawskiej. Można dodać odrobinę oliwy z oliwek gdyby się nie kleił.
      <strong> Dodatki</strong>: co Dusza zapragnie : roszponka, szpinak, ogórek, pomidor, awokado, szczypiorek, mango, szparagi,
      marchewka itd. w zasadzie wszystko się nada.
      Układamy, zawijamy , kroimy na mniejsze kawałeczki i zajadamy się :)
    </>,
    imgSrc: "/images/recipes/sushi.jpg"
  }, {
    headerText: "SUROWY sernik z mango",
    description: <>
      Kto nie <strong>KOCHA</strong> słodkości ? Niech podniesie rękę do góry :)
      Kochani surowe desery są tak pyszne, że nie można tego opisać słowami , trzeba po prostu ich spróbować.
      <br/><br/><strong>SKŁADNIKI NA SPÓD CIASTA:</strong><br/>
      2 szklanki zblendowanych migdałów<br/>
      1 szklanka wymoczonych i wypestkowanych daktyli<br/>
      1/4 szklanki surowego kakao<br/>
      2 łyżki syropu z agawy<br/><br/>
      <strong>MASA SEROWA:</strong><br/>
      2 szklanki wymoczonych przez noc orzechów nerkowca<br/>
      1/2 szklanki mleczka kokosowego<br/>
      5 łyżek syropu z agawy<br/>
      świeżo wyciśnięty sok z cytryny<br/> ( ja kocham <strong>DUŻO</strong> cytryny wtedy jest taki kwaskowaty smak )<br/>
      1 łyżeczka ekstraktu waniliowego<br/>
      <br/>
      <strong>SOS MANGO</strong>:<br/>
      2 mango<br/>
      3 łyżki syropu z agawy<br/>
      <br/>
      <strong>WYKONANIE</strong> :<br/>
      <strong>SPÓD CIASTA:</strong><br/>
      Migdały zblenduj na mąkę i odłóż do miseczki resztę składników umieść w blenderze z zmiksuj do momentu
      rozdrobnienia daktyli. Przenieś do miski z mąką z migdałów i wymieszaj. Wyłóż ciasto do formy i odstaw do
      lodówki.
      <strong>MASA SEROWA :</strong><br/>
      Wszystkie składniki zmiksuj do momentu uzyskania jednolitej masy. Przelej masę na spód ciasta i odłóż na
      bok.<br/>
      <strong>SOS MANGO:</strong><br/>
      Zmiksuj wszystkie składniki w blenderze do uzyskania gładkiej masy. Wylej na wierzch masy serowej.
      Wstaw na kilka godzin do zamrażarki lub na całą noc do lodówki.
      Wyjmij z zamrażarki na 20-30 minut przed podaniem.
    </>,
    imgSrc: "/images/recipes/mango.jpg",
    cwiStyle:{margin:'17rem 0'}
  },
    {
      headerText: "Pomysł na SUROWĄ zupę krem :)",
      description: <>
        Za co kocham surowe posiłki ? Za prostotę, szybkość i łatwość przygotowania.<br/> No i najważniejsze za <strong>PYSZNY SMAK</strong>
        :) <br/><br/>
        Przygotowanie tej zupy krem zajmie Ci pięć minut. GWARANTUJE !<br/>
        <strong>SKŁADNIKI I PRZYGOTOWANIE :</strong><br/><br/>
        3-4 pomidory malinowe<br/>
        1 czerwona papryka<br/>
        1 awokado<br/>
        Szczypa soli, pieprzu ziołowego i słodkiej papryki<br/>
        Wszystkie składniki blendujemy dekorujemy szczypiorkiem, pietruszką czy kolendrą i jemy ❤️<br/>
      </>,
      imgSrc: "/images/recipes/soup.jpg"
    }]


  return <Layout title="Przepisy" link={"recipes"}>
    <Header text="Przepisy"/>
    {recipes.map((recipeElement, index) => <ContentWithImage {...recipeElement} revert={index % 2}/>)}
  </Layout>
}


export default Recipes